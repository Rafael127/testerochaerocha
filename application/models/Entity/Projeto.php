<?php
namespace Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Projeto
 *
 * @Entity
 * @Table(name="projeto")
 * @author Marcos Iran<marcosiran@gmail.com>
 */

class Projeto{
    public function __construct(){
        //$this->atividade = new ArrayCollection();
    }
    
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $id;
    
    /**
     * @var Collection
     * @OneToMany(targetEntity="Atividade", mappedBy="idProjeto", cascade={"remove"})
     */
    public $atividade;
    
    /**
     * @Column(name="descricao", type="string", length=255, nullable=false)
     */
    public $descricao;
    
    
    public function getId(){
        return $this->id;
    }
    
    public function getDescricao(){
        return $this->descricao;
    }
    
    public function setDescricao($descricao){
        $this->descricao = $descricao;
        return $this->descricao;
    }
}