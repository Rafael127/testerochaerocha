<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="adjust-nav">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".sidebar-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"> <img height="80" width="80"
				src="<?php echo base_url('includes/imagens/rochaerocha.jpg')?>" />
			</a>

		</div>


	</div>
</div>
<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="main-menu">
			<li
				<?php echo (isset($menu_ativo) and $menu_ativo == 'principal') ? "class='active-link'" : ''; ?>><a
				href="<?php echo (isset($menu_ativo) and $menu_ativo == 'principal') ? '#' : base_url('principal'); ?>"><i
					class="fa fa-desktop "></i>Principal</a></li>

			<li
				<?php echo (isset($menu_ativo) and $menu_ativo == 'projeto') ? "class='active-link'" : ''; ?>><a
				href="<?php echo (isset($menu_ativo) and $menu_ativo == 'projeto') ? '#' : base_url('projeto'); ?>"><i
					class="fa fa-users "></i>Listar Projetos</a></li>

			<li
				<?php echo (isset($menu_ativo) and $menu_ativo == 'atividade') ? "class='active-link'" : ''; ?>><a
				href="<?php echo (isset($menu_ativo) and $menu_ativo == 'atividade') ? '#' : base_url('atividade'); ?>"><i
					class="fa fa-users "></i>Listar Atividades</a></li>

		</ul>
	</div>

</nav>
<!-- /. NAV SIDE  -->