<?php
use Doctrine\ORM\Query\ResultSetMapping;

if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Atividade extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        /*
         * $dados['atividades'] = $this->doctrine->em->getconnection()
         * ->query('SELECT a.id, a.descricao, a.dataCadastro, p.descricao as projeto
         * FROM atividade a
         * JOIN projeto p ON p.id = a.idProjeto
         * ORDER BY a.dataCadastro DESC')
         * ->fetchAll(PDO::FETCH_CLASS);
         */
        $dados['menu_ativo'] = 'atividade';
        $dados['atividades'] = $this->doctrine->em->getRepository("Entity\Atividade")->findAll();
        $this->load->view('atividade/lista_atividade_view', $dados);
    }

    public function editar()
    {
        $dados['projetos'] = $this->doctrine->em->getRepository("Entity\Projeto")->findAll();
        $dados['atividade'] = $this->doctrine->em->getRepository("Entity\Atividade")->find($this->input->get('id'));
        $this->load->view('atividade/editar_atividade_view', $dados);
    }

    public function atualizar()
    {
        $this->form_validation->set_rules('id', 'id', 'trim|required|integer');
        $this->form_validation->set_rules('descricao', 'Descricao', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('id_projeto', 'Projeto', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $dados['projetos'] = $this->doctrine->em->getRepository("Entity\Projeto")->findAll();
            $dados['erros'] = validation_errors('<li>', '</li>');
            $this->load->view('atividade/editar_atividade_view', $dados);
        } else {
            $projeto = $this->doctrine->em->getRepository("Entity\Projeto")->find($this->input->post('id_projeto'));
            $atividade = $this->doctrine->em->getRepository("Entity\Atividade")->find($this->input->post('id'));
            $atividade->setIdProjeto($projeto);
            $atividade->setDescricao($this->input->post('descricao'));
            $this->doctrine->em->persist($atividade);
            $this->doctrine->em->flush();
            redirect('atividade', 'refresh');
        }
    }

    public function deletar()
    {
        $atividade = $this->doctrine->em->getRepository("Entity\Atividade")->find($this->input->get('id'));
        $this->doctrine->em->remove($atividade);
        $this->doctrine->em->flush();
        redirect('atividade', 'refresh');
    }

    public function visualizar()
    {
        $dados['atividade'] = $this->doctrine->em->getRepository("Entity\Atividade")->find($this->input->get('id'));
        
        $this->load->view('atividade/visualizar_atividade_view', $dados);
    }

    public function cadastrar()
    {
        $this->form_validation->set_rules('descricao', 'Descricao', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('id_projeto', 'Projeto', 'trim|required');
        $dados['projetos'] = $this->doctrine->em->getRepository("Entity\Projeto")->findAll();
        
        if ($this->form_validation->run() == FALSE) {
            $dados['erros'] = validation_errors('<li>', '</li>');
            $this->load->view('atividade/cadastrar_atividade_view', $dados);
        } else {
            $atividade = new Entity\Atividade();
            $projeto = $this->doctrine->em->getRepository("Entity\Projeto")->find($this->input->post('id_projeto'));
            $atividade->setIdProjeto($projeto);
            $atividade->setDescricao($this->input->post('descricao'));
            $atividade->setDataCadastro(date("Y-m-d H:i:s"));
            $this->doctrine->em->persist($atividade);
            $this->doctrine->em->flush();
            redirect('atividade', 'refresh');
        }
    }
}