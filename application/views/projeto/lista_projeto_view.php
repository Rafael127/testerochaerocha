<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<link rel="stylesheet"
	href="<?php echo base_url('includes/assets/css/custom.css') ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" />
<script type="text/javascript"
	src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
        });
    });
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>

<link rel="stylesheet"
	href="<?php echo base_url('includes/assets/css/bootstrap.css') ?>">
<link rel="stylesheet"
	href="<?php echo base_url('includes/assets/css/custom.css') ?>" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
</head>
<body>
	<div id="wrapper">
		<?php $this->load->view('/commons/menu'); ?>	
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row" style="margin: 1px;">

					<div>
						<br> <a class="btn btn-primary"
							href="<?php echo base_url('/projeto/cadastrar'); ?>"><i
							class="fas fa-plus"></i> Novo Projeto</a> <br> <br> <strong>
			  <?php
    echo $this->session->flashdata('msg');
    ?>
    </strong>
						<table id="example" class="display" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>id</th>
									<th>descricaao</th>
								</tr>
							</thead>

                    <?php if ($projetos == null) { ?>	
                        <tbody>
								<tr>
									<td>nenhum registro encontrado!</td>
									<td></td>

								</tr>
							</tbody>
                    <?php } else { ?>
                        <tbody>
                            <?php foreach ($projetos as $linha) { ?>	
                                <tr>
									<td><?= $linha->id ?></td>
									<td><?= $linha->descricao ?></td>
									<td>
										<div class="row">
											<div class="col-md-12">
												<a class="btn btn-success btn-xs"
													href="<?php echo base_url("/projeto/visualizar?id=".$linha->id); ?>">
													<i class="fas fa-glasses"></i> Detalhar
												</a> <a class="btn btn-warning btn-xs"
													href="<?php echo base_url("/projeto/editar?id=".$linha->id); ?>">
													<i class="far fa-edit"></i> Editar
												</a> <a class="btn btn-danger  btn-xs"
													href="<?php echo base_url("/projeto/deletar?id=".$linha->id); ?>">
													<i class="far fa-trash-alt"></i> Deletar
												</a>
											</div>
										</div>
									</td>
								</tr>
                            <?php } ?>	
                        </tbody>
                    <?php } ?>	
                </table>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
                // For demo to fit into DataTables site builder...
                $('#example')
                        .removeClass('display')
                        .addClass('table table-striped table-bordered');
            </script>
	</div>
	<?php $this->load->view('commons/rodape'); ?>
</body>
</html>