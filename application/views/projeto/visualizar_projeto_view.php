<!DOCTYPE html>
<html lang="pt-br">
<head>
<link rel="stylesheet"
	href="<?php echo base_url('includes/assets/css/custom.css') ?>" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans'
	rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css" />
<script type="text/javascript"
	src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#example').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
        });
    });
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>

<link rel="stylesheet"
	href="<?php echo base_url('includes/assets/css/bootstrap.css') ?>">
<link rel="stylesheet"
	href="<?php echo base_url('includes/assets/css/custom.css') ?>" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
</head>
<body>
	<div id="wrapper">
		<?php $this->load->view('commons/menu'); ?>
		<div id="page-wrapper">
			<div id="page-inner">
				<div class="row" style="margin: 1px;">
					<h3>
						<strong>Dados</strong>
					</h3>
					<hr>
					<div class="form-group">
						<div class="row">
							<div class="form-group col-md-4">
								<p>
									<strong>Descrição: </strong><?php echo $projeto->descricao; ?> 
							</p>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('commons/rodape'); ?>
</body>
</html>