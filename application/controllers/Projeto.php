<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

class Projeto extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $dados['menu_ativo'] = 'projeto';
        $dados['projetos'] = $this->doctrine->em->getRepository("Entity\Projeto")->findBy(array(), array(
            "descricao" => "asc"
        ));
  
        $this->load->view('projeto/lista_projeto_view', $dados);
    }

    public function editar()
    {
        $dados['projeto'] = $this->doctrine->em->getRepository("Entity\Projeto")->find($this->input->get('id'));
        $this->load->view('projeto/editar_projeto_view', $dados);
    }

    public function atualizar()
    {
        $this->form_validation->set_rules('id', 'id', 'trim|required|integer');
        $this->form_validation->set_rules('descricao', 'Descricao', 'trim|required|min_length[3]');
        
        if ($this->form_validation->run() == FALSE) {
            $dados['erros'] = validation_errors('<li>', '</li>');
            $this->load->view('projeto/editar_projeto_view', $dados);
        } else {
            $projeto = $this->doctrine->em->getRepository("Entity\Projeto")->find($this->input->post('id'));
            $projeto->setDescricao($this->input->post('descricao'));
            $this->doctrine->em->persist($projeto);
            $this->doctrine->em->flush();
            redirect('projeto', 'refresh');
        }
    }

    public function deletar()
    {
        $projeto = $this->doctrine->em->getRepository("Entity\Projeto")->find($this->input->get('id'));
        
        $this->doctrine->em->remove($projeto);
        $this->doctrine->em->flush();
        redirect('projeto', 'refresh');
    }

    public function visualizar()
    {
        $dados['projeto'] = $this->doctrine->em->getRepository("Entity\Projeto")->find($this->input->get('id'));
        $this->load->view('projeto/visualizar_projeto_view', $dados);
    }

    public function cadastrar()
    {
        $this->form_validation->set_rules('descricao', 'Descricao', 'trim|required|min_length[3]');
        
        if ($this->form_validation->run() == FALSE) {
            $dados['erros'] = validation_errors('<li>', '</li>');
            $this->load->view('projeto/cadastrar_projeto_view', $dados);
        } else {
            $projeto = new Entity\Projeto();
            $projeto->setDescricao($this->input->post('descricao'));
            $this->doctrine->em->persist($projeto);
            $this->doctrine->em->flush();
            redirect('projeto', 'refresh');
        }
    }
}
